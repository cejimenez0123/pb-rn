import { createApi, fakeBaseQuery } from '@reduxjs/toolkit/query/react';
import { db } from '../core/di';
import Page from "../domain/models/page"
import { FirebaseFirestoreTypes } from '@react-native-firebase/firestore';
export const firestoreApi = createApi({
    baseQuery: fakeBaseQuery(),
    tagTypes: ['page'],
    endpoints: (builder) => ({
      fetchHighScoresTables: builder.query<FirebaseFirestoreTypes.CollectionReference, void>({
        async queryFn() {
          try {
            const snapshot = await db.collection(Page.name);

            
            // let scoresTables: ScoresTables = [];
            // querySnapshot?.forEach((doc) => {
            //   scoresTables.push({ id: doc.id, ...doc.data() } as ScoresTable);
            // });
            return { data: snapshot };
          } catch (error: any) {
            console.error(error.message);
            return { error: error.message };
          }
        },
        providesTags: ['page'],
      }),
      getPage: builder.query<Page,string>({
        async queryFn(id:string){
            try{
                const doc = db.collection(Page.name).doc(id).get()

                return { data: new Page("","","","","",false)}
            }catch (error: any) {
                console.error(error.message);
                return { error: error.message };
            }
        }})
    }),
});
    //   getPage:builder.query<Page,void>({
       
    //   })
    //   setNewHighScore: builder.mutation({
    //     async queryFn({ scoresTableId, newHighScore }) {
    //       try {
    //         await updateDoc(doc(firestore, 'scoresTables', scoresTableId), {
    //           scores: arrayUnion(newHighScore),
    //         });
    //         return { data: null };
    //       } catch (error: any) {
    //         console.error(error.message);
    //         return { error: error.message };
    //       }
    //     },
    //     invalidatesTags: ['Score'],
    //   }),
  
  
  export const {
    useFetchHighScoresTablesQuery,
    useSetNewHighScoreMutation,
  } = firestoreApi;

function queryFn(): any {
    throw new Error('Function not implemented.');
}
